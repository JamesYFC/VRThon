﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Composer : MonoBehaviour {

    public string songName;
    public string difficultyStr;

    private StepManiaSong song;
    private float currentBpm;
    private int bpmCounter;
    private float nextBpmChangeTime;
    private float songTime;
    private int measureCounter;
    private int stepCounter;
    private int beatsBeforeStartCountdown;
    private float nextBeatTime;
    private bool finishedSong;

    public LaneManager[] lanes;
    
    const int SECS_PER_MIN = 60;
    const int BEATS_BEFORE_START = 8;

    private void Awake() {
        
        string folderPath = Path.Combine(Application.dataPath, "Songs\\" + songName);
        song = new StepManiaSong(folderPath);

        bpmCounter = -1;
        UpdateBpms();
        songTime = 0;
        measureCounter = 0;
        stepCounter = 0;
        nextBeatTime = 0;
        finishedSong = false;
        beatsBeforeStartCountdown = BEATS_BEFORE_START;
    }
    // Use this for initialization
    void Start () {
	}

    void UpdateBpms() {
        bpmCounter += 1;
        currentBpm = song.meta.bpms[bpmCounter];
        if (bpmCounter + 1 == song.meta.bpmTimes.Count) {
            nextBpmChangeTime = float.MaxValue;
        } else {
            nextBpmChangeTime = song.meta.bpmTimes[bpmCounter + 1];
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!finishedSong) {
            songTime += Time.deltaTime;

            if (songTime >= nextBpmChangeTime) {
                UpdateBpms();
            }

            if (songTime >= nextBeatTime) {
                FireBeat();
            }
        }
	}

    void FireBeat() {
        if (beatsBeforeStartCountdown == 0) {
            GetComponent<AudioSource>().PlayDelayed(song.meta.offset);
            beatsBeforeStartCountdown = -1;
        } else {
            beatsBeforeStartCountdown -= 1;
        }
        StepController();
    }

    void StepController() {
        List<List<StepRow>> measures = song.steps[difficultyStr];
        if (measureCounter < measures.Count) {
            List<StepRow> measure = song.steps[difficultyStr][measureCounter];
            int stepsPerBeat = measure.Count / 4;

            StepRow spawnRow = measure[stepCounter];
            float[] timings = Enumerable.Repeat(SECS_PER_MIN / currentBpm, 8).ToArray();

            if (spawnRow.left) {
                lanes[0].Spawn(timings);
            }
            if (spawnRow.up) {
                lanes[1].Spawn(timings);
            }
            if (spawnRow.down) {
                lanes[2].Spawn(timings);
            }
            if (spawnRow.right) {
                lanes[3].Spawn(timings);
            }

            stepCounter += 1;
            nextBeatTime += SECS_PER_MIN / currentBpm / stepsPerBeat;

            if (stepCounter == measure.Count) {
                stepCounter = 0;
                measureCounter += 1;
            }
        } else {
            finishedSong = true;
        }
    }
}
