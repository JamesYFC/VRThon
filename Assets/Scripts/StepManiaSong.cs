﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;    //This structure contains all the information for this track

//This structure contains note information for a single 'row' of notes
//Right now it's just a simple "Is there a note there or not"
//But this could be modified and expanded to support numerous note types
public struct StepRow {
    public bool left;
    public bool right;
    public bool up;
    public bool down;
}

public class StepManiaSong {

    public struct Meta {
        //The Title, Subtitle and Artist for the song
        public string title;
        public string subtitle;
        public string artist;

        //The file paths for the related images and song media
        public string bannerPath;
        public string backgroundPath;
        public string musicPath;

        //The offset that the song starts at compared to the step info
        public float offset;

        //The start and length of the sample that is played when selecting a song
        public float sampleStart;
        public float sampleLength;

        //The bpms the song is played at at specific times
        public List<float> bpmTimes;
        public List<float> bpms;
    };

    //The note data for each difficulty, 
    //as well as a boolean to check that data for that difficulty exists
    public Meta meta;
    public Dictionary<string, List<List<StepRow>>> steps;

    public StepManiaSong(string folderPath) {
        string filePathName = Path.GetFileName(folderPath);
        string stepManiaFileName = Path.Combine(folderPath, filePathName + ".sm");
        this.steps = new Dictionary<string, List<List<StepRow>>>();
        string[] lines = File.ReadAllLines(stepManiaFileName);

        // Parse metadata
        this.ParseSongData(ref lines);
    }

    private bool ParseBool(char boolChar) {
        switch (boolChar) {
            case '0':
                return false;
            case '1':
                return true;
            case '2':
                return true;
            case '3':
                return true;
                
            default:
                return false;
        }
    }

    private void ParseSongData(ref string[] lines) {
        for (uint i = 0; i < lines.Length; i++) {
            string line = lines[i];
            string[] metadata = line.Split(':');

            string key = metadata[0];

            switch (key) {
                case "#TITLE":
                    this.meta.title = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#SUBTITLE":
                    this.meta.subtitle = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#ARTIST":
                    this.meta.artist = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#BANNER":
                    this.meta.bannerPath = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#BACKGROUND":
                    this.meta.backgroundPath = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#MUSIC":
                    this.meta.musicPath = metadata[1].Substring(0, metadata[1].Length - 1);
                    break;
                case "#OFFSET":
                    this.meta.offset = float.Parse(metadata[1].Substring(0, metadata[1].Length - 1));
                    break;
                case "#SAMPLESTART":
                    this.meta.sampleStart = float.Parse(metadata[1].Substring(0, metadata[1].Length - 1));
                    break;
                case "#SAMPLELENGTH":
                    this.meta.sampleLength = float.Parse(metadata[1].Substring(0, metadata[1].Length - 1));
                    break;
                case "#BPMS":
                    this.meta.bpmTimes = new List<float>();
                    this.meta.bpms = new List<float>();

                    foreach (string bpmChange in metadata[1].Substring(0, metadata[1].Length - 1).Split(',')) {
                        string[] bpmSplit = bpmChange.Split('=');
                        this.meta.bpmTimes.Add(float.Parse(bpmSplit[0]));
                        this.meta.bpms.Add(float.Parse(bpmSplit[1]));
                    }
                    break;
                case "#NOTES":
                    i = this.ParseDanceSingleSteps(ref lines, i + 1);
                    break;
            }
        }
    }

    private uint ParseDanceSingleSteps(ref string[] lines, uint lineCounter) {
        // first line is either dance-single: or dance-double:
        // only accept dance-single for now
        if (lines[lineCounter].Contains("dance-single")) {
            // move to the difficulty key
            lineCounter += 2;

            // might not be according to spec. strip the last character and trip whitespace
            string difficultyKey = lines[lineCounter].Substring(0, lines[lineCounter].Length - 1).Trim();
            this.steps[difficultyKey] = new List<List<StepRow>>();

            // move to beginning of the notes list
            lineCounter += 3;

            List<StepRow> currentBar = new List<StepRow>();

            // parse notes until a line ends with the ';' sigil
            while (!lines[lineCounter].EndsWith(";")) {
                string line = lines[lineCounter];
                // if a ',' is encountered, insert the bar
                if (line.StartsWith(",")) {
                    this.steps[difficultyKey].Add(currentBar);
                    currentBar = new List<StepRow>();
                    lineCounter += 1;
                } else if (line == "") {
                    lineCounter += 1;
                } else {
                    StepRow stepRow = new StepRow();
                    stepRow.left = ParseBool(line[0]);
                    stepRow.up = ParseBool(line[1]);
                    stepRow.down = ParseBool(line[2]);
                    stepRow.right = ParseBool(line[3]);

                    currentBar.Add(stepRow);
                    lineCounter += 1;
                }
            }
        } else {
            // fast-forward to a line with just the ';' symbol
            while(!lines[lineCounter].EndsWith(";")) {
                lineCounter += 1;
            }
            lineCounter += 1;
        }

        return lineCounter;
    }
}

public class SongLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
