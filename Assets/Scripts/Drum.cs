﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drum : MonoBehaviour {
    public LaneManager laneManager;
    private float lastHit;
    private void Start()
    {
        lastHit = Time.time;
    }
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        if (other.tag == "whacker")
        {
            if (Time.time > lastHit + .1)
            {

                laneManager.Hit();
                lastHit = Time.time;
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
