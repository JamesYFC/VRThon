﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {
    public Transform[] transforms;
    private LTSeq seq;
    
    private List<Vector3> positions = new List<Vector3>();
    private void Awake()
    {
        seq = LeanTween.sequence();
        Debug.Log("not added yet");
        foreach (Transform transform in transforms)
        {
            positions.Add(transform.position);
        }
    }
    void Start()
    {


    }
    
    public void SetTweens(float[] timings)
    {
        seq.append(LeanTween.color(gameObject, Color.green, 0));
        seq.append(timings[0] * 2.5f);
        seq.append(LeanTween.move(gameObject, new Vector3[] { transform.position, positions[0], positions[0], positions[1] }, timings[1]).setEase(LeanTweenType.easeOutQuad));
        seq.append(timings[2]);
        seq.append(LeanTween.move(gameObject, new Vector3[] { positions[1], positions[2], positions[2], positions[3] }, timings[3]).setEase(LeanTweenType.easeOutQuad));
        seq.append(timings[4]);
        seq.append(LeanTween.color(gameObject, Color.yellow, 0));
        seq.append(LeanTween.move(gameObject, new Vector3[] { positions[3], positions[2], positions[2], positions[4] }, timings[5]).setEase(LeanTweenType.easeOutQuad));
        seq.append(timings[6]);
        seq.append(LeanTween.color(gameObject, Color.red, 0));
        seq.append(LeanTween.move(gameObject, positions[6], timings[7]).setEase(LeanTweenType.easeOutQuad).setDestroyOnComplete(true));
    }
}
