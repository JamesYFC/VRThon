﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneManager : MonoBehaviour
{
    //public Transform[] transforms;
    Dictionary<GameObject, float> timings = new Dictionary<GameObject, float>();
    public GameObject prefab;
    public GameObject effect;
    private float lastHit = 0;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            Hit();
        }
    }
    public void Spawn(float[] timings)
    {
        float sum = 0;
        for(int i=0; i<=7; i++)
        {
            sum += timings[i];
        }
        GameObject spawn = Instantiate(prefab, transform);
        Debug.Log(prefab.transform.position);
        spawn.transform.SetParent(this.transform);
        spawn.SetActive(true);
        spawn.GetComponent<Move>().SetTweens(timings);
        this.timings.Add(spawn, Time.time + sum);
    }

    public void Hit()
    {


            foreach (KeyValuePair<GameObject, float> pair in timings)
            {
                if (Time.time > pair.Value)
                {
                    if (pair.Key != null)
                    {
                        GameObject displayEffect = Instantiate(effect);
                        displayEffect.transform.position = pair.Key.transform.position;
                        displayEffect.GetComponent<ParticleSystem>().Play();
                        Destroy(displayEffect, 1);
                    }
                    Destroy(pair.Key);
                    timings.Remove(pair.Key);
                    return;
                }
            }
    }
}
